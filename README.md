# vm-power.sh
This simple script is helpful in managing VMs' power operations on a KVM/LibVirt host. e.g. when you have to power up a large number of VMs , matching a pattern, perhaps a Kubernetes cluster! 

The script gets a list of available VMs, filters the list based on the pattern you mentioned on the CLI and then performs the power operation on those VMs.

# Setup:
Clone the repo and copy this script to your PATH, e.g. in `/usr/local/bin/` . You are expected to be `root` or use `sudo` to run this script. 


# Power operations:
* start - starts / power-on an existing VM.
* shutdown - (graceful) shutdown / power-off a running VM. The VM is expected to understand ACPI signals.
* destroy - (un-graceful) shutdown / power-off a running VM. Like pulling out a power plug abrubtly. **Doesn't delete a VM.**
* reboot - (graceful) reboot  a running VM. The VM is expected to understand ACPI signals.
* reset - (un-graceful) reboot / hard reset a running VM.


# Example runs:

## No parameters:
Running it without any parameters shows usage help as well as shows power status of all VMs on your LibVirt host.
```
[kamran@kworkhorse LibVirt]$ ./vm-power.sh 

./vm-power.sh <start|shutdown|destroy|reset|reboot> <name-pattern>

Example: ./vm-power.sh start example.com
         This will start all VMs which have example.com in their VM name


Here is the list of VMs though:

 Id    Name                           State
----------------------------------------------------
 1     DockerHost-ocdd                running
 -     CentOS-7                       shut off
 -     controller1.praqma.local       shut off
 -     controller2.praqma.local       shut off
 -     etcd1.praqma.local             shut off
 -     etcd2.praqma.local             shut off
 -     etcd3.praqma.local             shut off
 -     lb1.praqma.local               shut off
 -     lb2.praqma.local               shut off
 -     win10                          shut off
 -     worker1.praqma.local           shut off
 -     worker2.praqma.local           shut off


[kamran@kworkhorse LibVirt]$
```

## start
Running it with `start` operation looks like the following. It sleeps for three seconds after performing an operation on a VM.
```
[kamran@kworkhorse LibVirt]$ ./vm-power.sh start praqma.local

Executing: virsh start controller1.praqma.local
Domain controller1.praqma.local started

Executing: virsh start controller2.praqma.local
Domain controller2.praqma.local started

Executing: virsh start etcd1.praqma.local
Domain etcd1.praqma.local started

Executing: virsh start etcd2.praqma.local
Domain etcd2.praqma.local started

Executing: virsh start etcd3.praqma.local
Domain etcd3.praqma.local started

Executing: virsh start lb1.praqma.local
Domain lb1.praqma.local started

Executing: virsh start lb2.praqma.local
Domain lb2.praqma.local started

Executing: virsh start worker1.praqma.local
Domain worker1.praqma.local started

Executing: virsh start worker2.praqma.local
Domain worker2.praqma.local started


Here is the list of VMs after 'start' was performed on the VMs matching the name pattern 'praqma.local':

 Id    Name                           State
----------------------------------------------------
 1     DockerHost-ocdd                running
 2     controller1.praqma.local       running
 3     controller2.praqma.local       running
 4     etcd1.praqma.local             running
 5     etcd2.praqma.local             running
 6     etcd3.praqma.local             running
 7     lb1.praqma.local               running
 8     lb2.praqma.local               running
 9     worker1.praqma.local           running
 10    worker2.praqma.local           running
 -     CentOS-7                       shut off
 -     win10                          shut off

[kamran@kworkhorse LibVirt]$
```

## shutdown:
Running it with `shutdown` operation looks like the following. It sleeps for three seconds after performing an operation on a VM.
```
[kamran@kworkhorse LibVirt]$ ./vm-power.sh shutdown praqma.local

Executing: virsh shutdown controller1.praqma.local
Domain controller1.praqma.local is being shutdown

Executing: virsh shutdown controller2.praqma.local
Domain controller2.praqma.local is being shutdown

Executing: virsh shutdown etcd1.praqma.local
Domain etcd1.praqma.local is being shutdown

Executing: virsh shutdown etcd2.praqma.local
Domain etcd2.praqma.local is being shutdown

Executing: virsh shutdown etcd3.praqma.local
Domain etcd3.praqma.local is being shutdown

Executing: virsh shutdown lb1.praqma.local
Domain lb1.praqma.local is being shutdown

Executing: virsh shutdown lb2.praqma.local
Domain lb2.praqma.local is being shutdown

Executing: virsh shutdown worker1.praqma.local
Domain worker1.praqma.local is being shutdown

Executing: virsh shutdown worker2.praqma.local
Domain worker2.praqma.local is being shutdown


Here is the list of VMs after 'shutdown' was performed on the VMs matching the name pattern 'praqma.local':

 Id    Name                           State
----------------------------------------------------
 1     DockerHost-ocdd                running
 9     worker1.praqma.local           running
 10    worker2.praqma.local           running
 -     CentOS-7                       shut off
 -     controller1.praqma.local       shut off
 -     controller2.praqma.local       shut off
 -     etcd1.praqma.local             shut off
 -     etcd2.praqma.local             shut off
 -     etcd3.praqma.local             shut off
 -     lb1.praqma.local               shut off
 -     lb2.praqma.local               shut off
 -     win10                          shut off

[kamran@kworkhorse LibVirt]$ 
```
Sometimes the VMs may take extra time to actually switch off when `shutdown` is used. The output of the script may show some still running, but they might be in a process of shutting down internal processes. That is, if they understand and acknowledge ACPI. As in the output shown above, where the worker nodes still seem to be running! 

Otherwise you can always use `destroy`.


Here is the output of running this script without any options to get the status of all VMs:
```
[kamran@kworkhorse LibVirt]$ ./vm-power.sh

./vm-power.sh <start|shutdown|destroy|reset|reboot> <name-pattern>

Example: ./vm-power.sh start example.com
         This will start all VMs which have example.com in their VM name


Here is the list of VMs though:

 Id    Name                           State
----------------------------------------------------
 1     DockerHost-ocdd                running
 -     CentOS-7                       shut off
 -     controller1.praqma.local       shut off
 -     controller2.praqma.local       shut off
 -     etcd1.praqma.local             shut off
 -     etcd2.praqma.local             shut off
 -     etcd3.praqma.local             shut off
 -     lb1.praqma.local               shut off
 -     lb2.praqma.local               shut off
 -     win10                          shut off
 -     worker1.praqma.local           shut off
 -     worker2.praqma.local           shut off


[kamran@kworkhorse LibVirt]$ 
```

