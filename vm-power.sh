#!/bin/bash

ACTION=$1
PATTERN=$2

echo 

if [ -z "${ACTION}" ] ||  [ -z "${PATTERN}" ] ; then
  echo "$0 <start|shutdown|destroy|reset|reboot> <name-pattern>"
  echo
  echo "Example: $0 start example.com"
  echo "         This will start all VMs which have example.com in their VM name"
  echo
  echo 
  echo "Here is the list of VMs though:"
  echo
  virsh list --all
  echo
  exit 1
fi



VM_LIST=$(virsh list --all | grep "$PATTERN" | awk '{print $2}'| sort )



for VM in ${VM_LIST}; do
  echo "Executing: virsh ${ACTION} ${VM}"
  virsh ${ACTION} ${VM}
  # It is good to give the hypervisor a chance, so sleeping for few seconds is a good thing to do.
  sleep 3
done

sleep 2
echo 
echo "Here is the list of VMs after '${ACTION}' was performed on the VMs matching the name pattern '${PATTERN}':"
echo
virsh list --all


